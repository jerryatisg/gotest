package main

import (
	"encoding/json"
	"fmt"
	"gotest/mod1"
	"gotest/mod2"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

type people struct {
	Number  int    `json:"number"`
	Message string `json:"message"`
}

func main() {
	var wg sync.WaitGroup
	fmt.Println("main")
	mod1.Print()
	mod2.Print()

	getdata := func() {
		defer wg.Done()
		url := "http://api.open-notify.org/astros.json"

		spaceClient := http.Client{
			Timeout: time.Second * 2, // Timeout after 2 seconds
		}

		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			log.Fatal(err)
		}

		req.Header.Set("User-Agent", "jj-gotest")

		fmt.Printf("sending request to %s ....\n", url)
		res, getErr := spaceClient.Do(req)
		if getErr != nil {
			log.Fatal(getErr)
		}

		if res.Body != nil {
			defer res.Body.Close()
		}

		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			log.Fatal(readErr)
		}

		fmt.Println(".....got result")

		people1 := people{}
		jsonErr := json.Unmarshal(body, &people1)
		if jsonErr != nil {
			log.Fatal(jsonErr)
		}

		fmt.Printf("total people %d\n", people1.Number)
		fmt.Printf("message :%s\n", people1.Message)
	}

	wg.Add(1)
	go getdata()

	fmt.Println("waiting for api calls")

	wg.Wait()

	fmt.Println("finally done")
	// text := `{"people": [{"craft": "ISS", "name": "Sergey Rizhikov"}, {"craft": "ISS", "name": "Andrey Borisenko"}, {"craft": "ISS", "name": "Shane Kimbrough"}, {"craft": "ISS", "name": "Oleg Novitskiy"}, {"craft": "ISS", "name": "Thomas Pesquet"}, {"craft": "ISS", "name": "Peggy Whitson"}], "message": "success", "number": 6}`
	// textBytes := []byte(text)

	// people1 := people{}
	// err := json.Unmarshal(textBytes, &people1)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// fmt.Println(people1.Number)
	// fmt.Println(people1.Message)
}
